﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {

	[SerializeField]
	private Text NextPush;

	[SerializeField]
	private GameObject TitleText;

	[SerializeField]
	private Text TimeScore;

	[SerializeField]
	private Text Times;

	[SerializeField]
	private GameObject GoalText;

    [SerializeField]
    private GameObject TweetButton;

    [SerializeField]
    private AudioSource BGM;

    [SerializeField]
    private AudioClip[] BGM_clip;

	public static bool GoGame;

	public static int Point;

	public static float Nowtime;

    bool InStart=false;



	// Use this for initialization
	void Start () {

		NextPush.text="NextA";
		NextPush.enabled = false;
		Nowtime = 0;
		Times.enabled = false;
		GoalText.SetActive (false);
		TimeScore.enabled = false;
		GoGame = false;
        BGM.clip = BGM_clip[0];
        TweetButton.SetActive(false);


	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown ("space")&&!InStart) {
			TitleText.SetActive (false);
			GoGame = true;
			NextPush.enabled = true;
			Times.enabled = true;
            BGM.Play();
            InStart = false;
		}

		if (RingoRole.Gool_Flg) 
		{
			GoalText.SetActive (true);
			NextPush.enabled = false;
			Times.enabled = false;
			TimeScore.enabled = true;
			TimeScore.text = "ScoreTime : " + Nowtime.ToString ("F2");
            BGM.Stop();
            TweetButton.SetActive(true);
           

		}

        if (RingoRole.Gool_Flg&&Input.GetKeyDown ("space")) 
		{
			// 現在のシーン番号を取得
			int sceneIndex = SceneManager.GetActiveScene().buildIndex;
			SceneManager.LoadScene (sceneIndex,LoadSceneMode.Single);
			RingoRole.Gool_Flg = false;
            InStart = true;

		}


		if (GoGame) 
		{
			Times.text = "Time : " + Nowtime.ToString ("f2");

			if (!RingoRole.Gool_Flg) 
			{
				Nowtime += Time.deltaTime;
			}
		}

		if (RingoRole.Next_flg_A) 
		{
            NextPush.text = "Aだ！！";
		}

		if (RingoRole.Next_flg_W) 
		{
            NextPush.text = "Wだっ！";
		}
		if (RingoRole.Next_flg_D) 
		{
            NextPush.text="Dをおせぇ！";
		}
		if (RingoRole.Next_flg_S) 
		{
            NextPush.text = "ラストSだ！！";
		}
		
	}

    public void Tweet()
    {
        naichilab.UnityRoomTweet.Tweet ("korogaringo", "ころがりんごのタイム"+Nowtime+"秒!!", "unityroom", "unity1week");
    }



}
