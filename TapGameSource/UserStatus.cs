﻿/// <summary>
/// 作成者:中川良平
/// 日付　:1月27日
/// シート名:ユーザーステータススクリプト
/// </summary>
using UnityEngine;
using System.Collections;

public class UserStatus : MonoBehaviour {
	//ステージのアンロックしている数
	public static int	StageUnlock;
	//現在選択しているステージ
	public static string StageNow="";
	//クリアーフラグ
	private static bool created = false;
	//戻りフラグ
	public static bool GameBack;
	//広告削除状態
	public static int Ad_delete;
	//リトライ回数
	public static int Retry_num;
	//ハードモードかどうか
	public static bool Hard=false;

	// Use this for initialization
	void Start () {

		PlayerPrefs.Save();
		//StageUnlock = 13;
		PlayerPrefs.SetString("StageNameNow",StageNow);
		//PlayerPrefs.SetInt ("StageUnlock",StageUnlock);
		StageUnlock= PlayerPrefs.GetInt ("StageUnlock",0);
		Ad_delete = PlayerPrefs.GetInt ("AdDelete",0);
		Debug.Log ("Stage=" + StageNow);
		Debug.Log ("UnlockNo=" + StageUnlock);

		if (!created) {
			created = true;
			DontDestroyOnLoad (this.gameObject);
		} else 
		{
			Destroy (this.gameObject);
		}
	
	}
	
	// Update is called once per frame
	void Update () {

		//Debug.Log ("Mode="+ Hard);
	
	}

	public void UserStatusDlete()
	{
		PlayerPrefs.DeleteAll ();
	}
}


