﻿/// <summary>
/// 作成者:中川良平
/// 日付　:1月27日
/// シート名:ゲームマネージャースクリプト
/// </summary>
using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	[SerializeField]
	private GameObject Player;
	[SerializeField]
	private GameObject Stage;
	[SerializeField]
	private GameObject Ui;
	[SerializeField]
	private GameObject Ui_2;


	//フラグ系
	public bool Goal_flag;
	public bool Start_flag;
	public bool GameOver_flag;
	public bool Camera_flag;



	[SerializeField]
	private SceneMove scenemove;



	[SerializeField]
	AudioSource Se;

	[SerializeField]
	AudioSource BGM;

	[SerializeField]
	AudioClip[] SeClip;

	// Use this for initialization
	void Start () {
		


	}
	
	// Update is called once per frame
	void Update () {

		//スタート処理
		if(Input.GetMouseButtonDown(0)&&Start_flag==false)
		{
			StartGame ();
			//スタートジングルのためのディレイ
			Invoke ("PlayBGM",2.0f);
		}

		//ゴール処理
		if(Goal_flag)
		{
			Invoke("Goal",3.0f);
			BGM.Stop ();
		}
			
			
		//ゲームオーバー処理
		if (GameOver_flag) 
		{
			Invoke ("GameOver",0.5f);
			BGM.Stop ();
		}
	}


	public void PlayBGM()
	{
		BGM.Play ();
	}

	public void StartGame()
	{
		//Stage.SetActive (true);
		//Player.SetActive (true);
		Start_flag = true;
		Ui_2.SetActive (false);
		Debug.Log("Re=" + UserStatus.Retry_num);
		Se.PlayOneShot (SeClip [0]);
	}
		

	public void  GameOver()
	{
		//Stage.SetActive (false);
		Ui.SetActive (true);
		Player.SetActive (false);

	}

	public void Goal()
	{
		scenemove.GoClear();

	}


		
}
