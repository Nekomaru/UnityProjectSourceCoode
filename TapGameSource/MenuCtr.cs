﻿/// <summary>
/// 作成者:中川良平
/// 日付　:1月27日
/// シート名:メニュー制御スクリプト
/// </summary>
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuCtr : MonoBehaviour {
	
	//スライドの制御
	int Stage_No;
	public float speed;
	public float pos;
	public int   pos_max;
	public int 	 Stage_Count;
	public int 	 Stage_Max;
	bool  count_flg;

	//ボタンプレートのスライド制御用変数
	public int Buttonpanel_Count;
	bool b_count_flg;

	//各オブジェクト
	[SerializeField]
	private GameObject Panel;

	[SerializeField]
	private GameObject[] maru;

	[SerializeField]
	private GameObject info_stage;

	[SerializeField]
	private GameObject select_stage;

	[SerializeField]
	private GameObject StageButton_panel;

	[SerializeField]
	private GameObject Title;

	[SerializeField]
	private Button[] Stagebuttone;


	[SerializeField]
	private Button Ad_BuyButton;

	[SerializeField]
	private	Text	Text_AdDelete;

	[SerializeField]
	Button Hard_Button;

	[SerializeField]
	private Button[] Select_R;


	[SerializeField]
	private Button[] Select_L;

	//フラグ関係
	//移動フラグ
	bool R_flag;
	bool L_flag;

	//ボタン移動フラグ
	bool B_R_flag;
	bool B_L_flag;

	//フリック用意
	public float StartPos;
	public float EndPos;
	private Camera mainCamera;

	void Start()
	{

		mainCamera = GameObject.FindWithTag ("MainCamera").GetComponent<Camera>();

		count_flg = true;
		UserStatus.StageNow=PlayerPrefs.GetString ("StageNameNow", UserStatus.StageNow);
		UserStatus.StageUnlock = PlayerPrefs.GetInt ("StageUnlock", UserStatus.StageUnlock);

		//ステージアンロック判定
		StageUnlockr ();

		//ハードモード処理
		UserStatus.Hard = false;

	

		if (UserStatus.Ad_delete == 1) 
		{
			Text_AdDelete.text = "購入済み";
			Ad_BuyButton.interactable = false;
			PlayerPrefs.Save();
		}

		//広告削除
		PlayerPrefs.GetInt("AdDelete",0);

		if(UserStatus.GameBack)
		{
			Title.SetActive(false);
			select_stage.SetActive (true);
		}
	
	}

	void Update()
	{

		//フリック処理
		if (Input.GetMouseButtonDown (0)) 
		{
			StartPos = mainCamera.WorldToScreenPoint (Input.mousePosition).x;
		}
		if (Input.GetMouseButtonUp (0)) 
		{
			EndPos = mainCamera.WorldToScreenPoint (Input.mousePosition).x;
			if (StartPos > EndPos && info_stage.activeSelf == false) 
			{
				B_slide_Anime_R ();
			} else if (StartPos < EndPos && info_stage.activeSelf == false) 
			{
				B_slide_Anime_L ();
			}

			else if (StartPos > EndPos&&info_stage.activeSelf==true) 
			{
				slide_Anime_R ();
				//mainCamera.transform.position = new Vector3 (mainCamera.transform.position.x + 10, mainCamera.transform.position.y, -10);
			}
			else if (StartPos < EndPos&&info_stage.activeSelf==true) 
			{
				slide_Anime_L ();
				//mainCamera.transform.position = new Vector3 (mainCamera.transform.position.x - 10, mainCamera.transform.position.y, -10);
			}
			StartPos = 0;
			EndPos = 0;	
		}
			



		if (Stage_Count +1 <= UserStatus.StageUnlock||Stage_Count==14) 
		{
			Hard_Button.interactable = true;
		} else 
		{
			Hard_Button.interactable = false;
		}

		//ステージインフォ右ボタン
		if (R_flag) 
		{
			Panel.transform.position -= transform.right * speed;
			pos++;
		} //スライドを止める処理
		else if (pos >= pos_max) 
		{
			Stop ();
			pos = 0;
			count_flg = true;
		}


		//ステージインフォ左ボタン
		if (L_flag) 
		{
			Panel.transform.position += transform.right * speed;
			pos++;

		} //スライドを止める処理
		else if (pos >= pos_max) 
		{
			Stop ();
			pos = 0;
			count_flg = true;
		}



		//ボタンパネル右ボタン
		if (B_R_flag) 
		{
			//Panel.transform.position -= transform.right * speed;
			StageButton_panel.transform.position -= transform.right * speed;
			pos++;
		} //スライドを止める処理
		else if (pos >= pos_max) 
		{
			Stop ();
			pos = 0;
			count_flg = true;
		}


		//ボタンパネル左ボタン
		if (B_L_flag) 
		{
			//Panel.transform.position += transform.right * speed;
			StageButton_panel.transform.position += transform.right * speed;
			pos++;

		} //スライドを止める処理
		else if (pos >= pos_max) 
		{
			Stop ();
			pos = 0;
			count_flg = true;
		}
			
			
		switch (Buttonpanel_Count) 
		{
		case 0:
			//
			maru [0].GetComponent<Image> ().color = Color.yellow;
			maru [1].GetComponent<Image> ().color = Color.white;

			break;
		case 1:
			//
			maru [0].GetComponent<Image> ().color = Color.white;
			maru [1].GetComponent<Image> ().color = Color.yellow;
			break;
		}

		//ボタンの有効か(左)
		if (Buttonpanel_Count == 0) {
			Select_L [0].interactable = false;
		} else 
		{
			Select_L [0].interactable = true;
		}

			
		if (Stage_Count == 0) {
			Select_L [1].interactable = false;
		} else 
		{
			Select_L [1].interactable = true;
		}

		//ボタンの有効か(右)
		if (Buttonpanel_Count == 1) {
			Select_R [0].interactable = false;
		} else 
		{
			Select_R [0].interactable = true;
		}

		if (Stage_Count == UserStatus.StageUnlock) 
		{
			Select_R [1].interactable = false;
		} else 
		{
			Select_R [1].interactable = true;
		}


			
	}
	//スライド処理forR
	public void slide_Anime_R()
	{
		//ステージ数分
		//if ((count_flg)&&Stage_Count < Stage_Max) 
		if ((count_flg)&&Stage_Count < UserStatus.StageUnlock ) 
		{
			R_flag = true;
			count_flg = false;
			Stage_Count++;
			UserStatus.StageNow= "Stage_"+Stage_Count;
			PlayerPrefs.SetString("StageNameNow",UserStatus.StageNow);
		}
			
	}

	//スライド処理forL
	public void slide_Anime_L()
	{
		if ((count_flg)&&Stage_Count > 0) 
		{
			L_flag = true;
			count_flg = false;
			Stage_Count--;
			UserStatus.StageNow = "Stage_"+Stage_Count;
			PlayerPrefs.SetString("StageNameNow",UserStatus.StageNow);
		}
	}


	//ボタンスライド処理forR
	public void B_slide_Anime_R()
	{
		//ステージ数分
		//if ((count_flg)&&Stage_Count < Stage_Max) 
		if ((count_flg)&&Buttonpanel_Count < 1 ) 
		{
			B_R_flag = true;
			count_flg = false;
			Buttonpanel_Count++;
		}

	}

	//ボタンスライド処理forL
	public void B_slide_Anime_L()
	{
		if ((count_flg)&&Buttonpanel_Count > 0) 
		{
			B_L_flag = true;
			count_flg = false;
			Buttonpanel_Count--;
		}
	}

	//スライドよとまれえええええ
	public void Stop()
	{
		R_flag = false;
		L_flag = false;
		B_R_flag = false;
		B_L_flag = false;
	}


	public void Cansel()
	{
		info_stage.SetActive (false);
		select_stage.SetActive (true);
		Stage_Count = 0;
		Panel.transform.localPosition=new Vector3 (-250, 0, 0);
	}

	public void StartButton()
	{
		Title.SetActive (false);
		select_stage.SetActive (true);
	}


	public void Hard()
	{
		UserStatus.Hard = true;
	}


	/// <summary>
	/// ここからステージの関数
	/// ステージを増やした際は必ずこのメソッドを追加する
	/// </summary>
	public void Stage1_1()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * 0;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString("StageNameNow",UserStatus.StageNow);
		Stage_Count += 0;
		
	}

	public void Stage1_2()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -100;
		Stage_Count += 1;
		UserStatus.StageNow = "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);

	}

	public void Stage1_3()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -200;
		Stage_Count += 2;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);

	}

	public void Stage2_1()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -300;
		Stage_Count += 3;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);

	}

	public void Stage2_2()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -400;
		Stage_Count += 4;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);

	}


	public void Stage2_3()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -500;
		Stage_Count += 5;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);
	}


	public void Stage3_1()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -600;
		Stage_Count += 6;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);

	}


	public void Stage3_2()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -700;
		Stage_Count += 7;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);
	}

	public void Stage3_3()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -800;
		Stage_Count += 8;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);
	}

	public void Stage4_1()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -900;
		Stage_Count += 9;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);
	}

	public void Stage4_2()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -1000;
		Stage_Count += 10;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);
	}

	public void Stage4_3()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -1100;
		Stage_Count += 11;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);
	}

	public void Stage5_1()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -1200;
		Stage_Count += 12;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);
	}

	public void Stage5_2()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -1300;
		Stage_Count += 13;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);
	}

	public void Stage5_3()
	{
		info_stage.SetActive (true);
		//select_stage.SetActive (false);
		Panel.transform.position += transform.right * -1400;
		Stage_Count += 14;
		UserStatus.StageNow= "Stage_"+Stage_Count;
		PlayerPrefs.SetString ("StageNameNow",UserStatus.StageNow);
	}

	/// <summary>
	/// ここまでしゃ
	/// </summary>

	//アンロックされているステージ数解放
	public void StageUnlockr()
	{
		for (int i = 0; i <= Stage_Max; i++) 
		{
			Stagebuttone [i].interactable = false;
		}



		for (int i = 0; i <= UserStatus.StageUnlock; i++) 
		{
			Stagebuttone [i].interactable = true;
		}
			
	}


}
